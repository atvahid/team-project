﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sweet_Cafe
{
    class UserAccount
    {
        /// <summary>
        /// The account number of the account
        /// </summary>
        private int _acctNumber;

        /// <summary>
        /// The username of the customer account
        /// </summary>
        private string _username;

        /// <summary>
        /// The password of the customer account
        /// </summary>
        private string _password;

        /// <summary>
        /// The required code of the customer's free beverage
        /// </summary>
        private int _code;

        /// <summary>
        /// The order of the customer
        /// </summary>
        private Order _order;

        /// <summary>
        /// The constructor of the UserAccount object 
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        public UserAccount(string username = "", string password = "")
        {
            //Initialize the username
            _username = username;

            //Initialize the password
            _password = password;

            //Initialize the code
            _code = 0;

            //Initialize the order
            _order = null;
        }

        public int AccountNumber
        {
            get { return _acctNumber; }
            set { _acctNumber = value; }
        }

        /// <summary>
        /// Create read-write properties for the username
        /// </summary>
        public string Username
        {
            get { return _username; }
            set { _username = value; }
        }

        /// <summary>
        /// Create the read-write properties for the password
        /// </summary>
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        /// <summary>
        /// Create the read-write properties for the code
        /// </summary>
        public int Code
        {
            get { return _code; }
            set { _code = value; }
        }

        /// <summary>
        /// Places the order, gets all information from UI and enters it into the order
        /// </summary>
        public void PlaceOrderAsUser()
        {

        }

        public void Save(StreamWriter writer)
        {
            //save the user account properties
            writer.WriteLine(_acctNumber);
            writer.WriteLine(_username);
            writer.WriteLine(_password);
        }
    }
}
