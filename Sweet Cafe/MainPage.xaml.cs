﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Sweet_Cafe
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        /// <summary>
        /// The list of user accounts
        /// </summary>
        private List<UserAccount> _user;

        /// <summary>
        /// The order of the customer, this is used if the customer doesn't have an account and just wants
        /// to order normally
        /// </summary>
        private Order _custOrder;

        public MainPage()
        {
            this.InitializeComponent();

            //Initialize the user accounts
            _user = new List<UserAccount>();
        }

        /// <summary>
        /// Finds the required account in the list of user accounts
        /// </summary>
        /// <param name="crtAcct"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        private UserAccount FindAccount(bool crtAcct, string username, string password)
        {
            return null;
        }

        private void OnLogin(object sender, RoutedEventArgs e)
        {
            //Make required options visible and not required options collapsed
            LoginOptions.Visibility = Visibility.Visible;
            CrtAccOpt.Visibility = Visibility.Collapsed;
        }

        private void OnCreateAccount(object sender, RoutedEventArgs e)
        {
            //Make required options visible and not required options collapsed
            CrtAccOpt.Visibility = Visibility.Visible;
            LoginOptions.Visibility = Visibility.Collapsed;
        }

        private void OnPlaceOrder(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MenuPage), _user);
            
        }

        private void OnCreateUserAccount(object sender, RoutedEventArgs e)
        {
            //make sure users input satisfies all conditions before creating the account
            if(_crtUsrName.Text == null || _crtPassName.Text == null || _confirmPassName.Text == null)
            {
                Debug.WriteLine("Please make sure all fields are filled out");
            }
            else if(_crtPassName.Text != _confirmPassName.Text)
            {
                Debug.WriteLine("Passwords do not match");
            }
            else
            {
                //read UI user input 
                string _username = _crtUsrName.Text;
                string _password = _crtPassName.Text;
        
                //create the account object
                UserAccount acct = new UserAccount(_username, _password);

                //save the account and add it to the account list
                _user.Add(acct);

                //make new options set visible and other option sets collapsed 
                NonUserOptions.Visibility = Visibility.Collapsed;
                CrtAccOpt.Visibility = Visibility.Collapsed;
                UserOptions.Visibility = Visibility.Visible;
            }
        }
    }
}