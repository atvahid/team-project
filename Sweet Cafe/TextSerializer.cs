﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sweet_Cafe
{
    class TextSerializer : UserAccountSerializer
    {
        public TextSerializer(AccountFileFormat fileFormat) : base(AccountFileFormat.TXT)
        {
        }

        public override void Load()
        {
            //obtain the list of account in the data folder
            string[] acctFileList = Directory.GetFiles(_acctDirPath);

            //go through every file, create an account and read the file content 
            foreach (string filePath in acctFileList)
            {
                using (StreamReader reader = new StreamReader(new FileStream(filePath, FileMode.Open)))
                {
                    //read the account type
                    string acctType = reader.ReadLine();

                    //TODO: create the account object based on the type read
                    UserAccount account = new UserAccount();
                }
            }
        }

        public override void Save()
        {
            //for every account in the list create a file and save the account to it
            foreach (UserAccount acct in _accountList)
            {
                //determine a file path
                string filePath = $"{_acctDirPath}/Acct{acct.AccountNumber}.dat";

                //create the file and save the account
                using (StreamWriter writer = new StreamWriter(new FileStream(filePath, FileMode.Create)))
                {
                    //write the type of account
                    writer.WriteLine(acct.GetType().ToString());

                    //ask the account to save itself using the given writer
                    acct.Save(writer);
                }
            }
        }
    }
}
