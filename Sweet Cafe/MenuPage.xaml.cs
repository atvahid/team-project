﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Sweet_Cafe
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MenuPage : Page
    {
        //The Item the customer wants 
        private Item _item;

        private Order _order;

        public MenuPage()
        {
            this.InitializeComponent();
        }

        private void rbtnPastry1_Checked(object sender, RoutedEventArgs e)
        {                 
            priceLabel.Visibility = Visibility.Visible;
            txtPrice.Visibility = Visibility.Visible; 
            txtPrice.Text = "$1.65";
          

           // _order.PastryOrder(Pastry.ApplePie);
        }

        private void rbtnPastry2_Checked(object sender, RoutedEventArgs e)
        {
            priceLabel.Visibility = Visibility.Visible;
            txtPrice.Visibility = Visibility.Visible;
            txtPrice.Text = "$1.75";
        }

        private void rbtnPastry3_Checked(object sender, RoutedEventArgs e)
        {
            priceLabel.Visibility = Visibility.Visible;
            txtPrice.Visibility = Visibility.Visible;
            txtPrice.Text = "$1.85";
        }

        private void rbtnPastry4_Checked(object sender, RoutedEventArgs e)
        {
            priceLabel.Visibility = Visibility.Visible;
            txtPrice.Visibility = Visibility.Visible;
            txtPrice.Text = "$1.90";
        }

        private void rbtnPastry5_Checked(object sender, RoutedEventArgs e)
        {
            priceLabel.Visibility = Visibility.Visible;
            txtPrice.Visibility = Visibility.Visible;
            txtPrice.Text = "$1.60";
        }

        private void rbtnPastry6_Checked(object sender, RoutedEventArgs e)
        {
            priceLabel.Visibility = Visibility.Visible;
            txtPrice.Visibility = Visibility.Visible;
            txtPrice.Text = "$1.70";
        }

      





        private void onClickPastry(object sender, RoutedEventArgs e)
        {
            rbtnPastry1.Visibility = Visibility.Visible;
            rbtnPastry2.Visibility = Visibility.Visible;
            rbtnPastry3.Visibility = Visibility.Visible;
            rbtnPastry4.Visibility = Visibility.Visible;
            rbtnPastry5.Visibility = Visibility.Visible;
            rbtnPastry6.Visibility = Visibility.Visible;

        }

        private void onClickBeverages(object sender, RoutedEventArgs e)
        {
            rbtnBeverage1.Visibility = Visibility.Visible;
            rbtnBeverage2.Visibility = Visibility.Visible;
            rbtnBeverage3.Visibility = Visibility.Visible;
            rbtnBeverage4.Visibility = Visibility.Visible;
            rbtnBeverage5.Visibility = Visibility.Visible;
            rbtnBeverage6.Visibility = Visibility.Visible;
            rbtnSmall.Visibility = Visibility.Visible;
            rbtnMedium.Visibility = Visibility.Visible;
            rbtnLarge.Visibility = Visibility.Visible;


        }

        private void OnPay(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(OrderPage), _item);
        }

        private void rbtnSmall_Checked(object sender, RoutedEventArgs e)
        {
            priceLabel.Visibility = Visibility.Visible;
            txtPrice.Visibility = Visibility.Visible;
            if (rbtnSmall.IsChecked == true)
            {
                txtPrice.Text = "1.70";
            }
        }

        private void rbtnMedium_Checked(object sender, RoutedEventArgs e)
        {
            priceLabel.Visibility = Visibility.Visible;
            txtPrice.Visibility = Visibility.Visible;
            if (rbtnMedium.IsChecked == true)
            {
                txtPrice.Text = "1.90";
            }

        }

        private void rbtnLarge_Checked(object sender, RoutedEventArgs e)
        {
            priceLabel.Visibility = Visibility.Visible;
            txtPrice.Visibility = Visibility.Visible;
            if (rbtnLarge.IsChecked == true)
            {
                txtPrice.Text = "2.10";
            }

        }

        
    }
}