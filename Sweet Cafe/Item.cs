﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sweet_Cafe 
{
    /// <summary>
    /// Represents the beverage type
    /// </summary>
    enum Beverage
    {
        Coffee = 1, 
        Tea,
        CaffeMocha,
        Latte,
        Cappuccino,
        Smoothie
    }

    enum BeverageSizes
    {
        Small = 1,
        Medium,
        Large
    }

    /// <summary>
    /// Represents the pastry type
    /// </summary>
    enum Pastry
    {
        ApplePie = 1,
        CreamPuff,
        Blueberry,
        Chocolate,
        CinnamonRoll,
        Cannoli
     
    }

    /// <summary>
    /// Represents the item of the menu, can either be a pastry or a beverage 
    /// </summary>
    class Item
    {
        /// <summary>
        /// The size of the item (pastry or beverage), can either be S, M, L
        /// </summary>
        private BeverageSizes _size;

        /// <summary>
        /// The type of pastry
        /// </summary>
        private Pastry _pastry;

        /// <summary>
        /// The type of beverage
        /// </summary>
        private Beverage _beverage;

        /// <summary>
        /// Constructor of the Item object for beverage
        /// </summary>
        /// <param name="beverage"></param>
        public Item(Beverage beverage)
        {

            //assign beverage option
            _beverage = beverage;

          
        }

        /// <summary>
        /// Constructor of the Item object for pastry
        /// </summary>
        /// <param name="pastry"></param>
        public Item(Pastry pastry)
        {
            //assign pastry option
            _pastry = pastry;
        }

        //Declare read-write properties for the size
        public Item (BeverageSizes size)
        {
            _size = size;
        }

        /// <summary>
        /// Calculates the price of the item based on the beverage options, beverage and pastry
        /// </summary>
        /// <returns></returns>
        public double CalculatePastryPrice()
        {
            double pastryPrice;
           if (_pastry == Pastry.ApplePie)
            {
                pastryPrice = 1.65;
                
               
            }
           else if (_pastry == Pastry.CreamPuff)
            {
                pastryPrice = 1.75;
            }

            else if (_pastry == Pastry.Blueberry)
            {
                pastryPrice = 1.85;
            }

            else if (_pastry == Pastry.Chocolate)
            {
                pastryPrice = 1.90;
            }

            else if (_pastry == Pastry.CinnamonRoll)
            {
                pastryPrice = 1.60;
            }

            else if (_pastry == Pastry.Cannoli)
            {
                pastryPrice = 1.70;
            }

            return CalculatePastryPrice();
            
  

        }

        /// <summary>
        /// Calculates the price of the beverage options
        /// </summary>
        /// <returns></returns>
        public double CalculateBevPrice()
        {
            double bevPrice;
              if (_beverage == Beverage.Coffee && _size == BeverageSizes.Small)
            {
                bevPrice = 1.70;
            }
              else if (_beverage == Beverage.Coffee && _size == BeverageSizes.Medium)
            {
                bevPrice = 1.90;
            }
                
            
            else if (_beverage == Beverage.Coffee && _size == BeverageSizes.Large)
            {
                bevPrice = 2.10;
            }

            if (_beverage == Beverage.Tea && _size == BeverageSizes.Small)
            {
                bevPrice = 1.70;
            }
            else if (_beverage == Beverage.Tea && _size == BeverageSizes.Medium)
            {
                bevPrice = 1.90;
            }


            else if (_beverage == Beverage.Tea && _size == BeverageSizes.Large)
            {
                bevPrice = 2.10;
            }


            if (_beverage == Beverage.CaffeMocha && _size == BeverageSizes.Small)
            {
                bevPrice = 1.70;
            }

            else if (_beverage == Beverage.CaffeMocha && _size == BeverageSizes.Medium)
            {
                bevPrice = 1.90;
            }


            else if (_beverage == Beverage.CaffeMocha && _size == BeverageSizes.Large)
            {
                bevPrice = 2.10;
            }


            if (_beverage == Beverage.Latte && _size == BeverageSizes.Small)
            {
                bevPrice = 1.70;
            }
            else if (_beverage == Beverage.Latte && _size == BeverageSizes.Medium)
            {
                bevPrice = 1.90;
            }


            else if (_beverage == Beverage.Latte && _size == BeverageSizes.Large)
            {
                bevPrice = 2.10;
            }

            if (_beverage == Beverage.Cappuccino && _size == BeverageSizes.Small)
            {
                bevPrice = 1.70;
            }
            else if (_beverage == Beverage.Cappuccino && _size == BeverageSizes.Medium)
            {
                bevPrice = 1.90;
            }


            else if (_beverage == Beverage.Cappuccino && _size == BeverageSizes.Large)
            {
                bevPrice = 2.10;
            }

            if (_beverage == Beverage.Smoothie && _size == BeverageSizes.Small)
            {
                bevPrice = 1.70;
            }
            else if (_beverage == Beverage.Coffee && _size == BeverageSizes.Medium)
            {
                bevPrice = 1.90;
            }


            else if (_beverage == Beverage.Coffee && _size == BeverageSizes.Large)
            {
                bevPrice = 2.10;
            }


            return CalculateBevPrice();
           
          
        } 
    }
}
