﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace Sweet_Cafe 
{
    /// <summary>
    /// Represents the order of the customer
    /// </summary>
    class Order : Page
    {
        /// <summary>
        /// The order number of the order
        /// </summary>
        private int _orderNumber;

        /// <summary>
        /// The item that is being ordered
        /// </summary>
        private Item _item;

        /// <summary>
        /// The randomizer that decides whether to give the customer the code to win the free beverage
        /// </summary>
        private Random _randomizer;

        /// <summary>
        /// Constructor of the order object
        /// </summary>
        /// <param name="orderNum"></param>
        public Order (int orderNum)
        {
            //assign the order number
            _orderNumber = orderNum;

            //Initialize the randomizer
            _randomizer = new Random();
        
        }

        //checks whether the customer has won the free beverage

        //public Prize();
        //int prizeNumber = _randomizer.Next(1, 50);

        //    if (prizeNumber > 0 && prizeNumber <= 10)
        //    {
                
        //    }

        public bool HasWonPrice
        {
           
            get { return false; }

            
        }

        /// <summary>
        /// Calculates the total price of the order 
        /// </summary>
        /// <returns></returns>
        public double CalaculateTotalPrice()
        {



            return 0;
            
            


        }

        /// <summary>
        /// The pastry order 
        /// </summary>
        /// <param name="pastryType"></param>
        public Item PastryOrder(Pastry pastryType)
        {
            if (pastryType == Pastry.ApplePie)
            {
                return null;
            }

            if (pastryType == Pastry.CreamPuff)
            {
                return null;
            }

            if (pastryType == Pastry.Blueberry)
            {
                return null;
            }

            if (pastryType == Pastry.Chocolate)
            {
                return null;
            }

            if (pastryType == Pastry.CinnamonRoll)
            {
                return null;
            }

            if (pastryType == Pastry.Cannoli)
            {
                return null;
            }
            return null;

            

        }

        /// <summary>
        /// The beverage order 
        /// </summary>
        /// <param name="beverageType"></param>
        public Item BeverageOrder(Beverage beverageType)
        {
            if (beverageType == Beverage.Coffee)
            {
                return null;
            }

            if (beverageType == Beverage.Tea)
            {
                return null;
            }

            if (beverageType == Beverage.CaffeMocha)
            {
                return null;
            }

            if (beverageType == Beverage.Latte)
            {
                return null;
            }

            if (beverageType == Beverage.Cappuccino)
            {
                return null;
            }

            if (beverageType == Beverage.Smoothie)
            {
                return null;
            }

            return null;
        }

        /// <summary>
        /// Calculates the hst for the order
        /// </summary>
        /// <returns></returns>
        public double CalculateHST()
        {
            double HST = CalaculateTotalPrice() * 0.13;
            double finalPrice = HST + CalaculateTotalPrice();
            return finalPrice;
        }

    }
}
