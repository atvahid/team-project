﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace Sweet_Cafe
{
    class JsonSerializer : UserAccountSerializer
    {
        public JsonSerializer(AccountFileFormat fileFormat) : base(AccountFileFormat.JSON)
        {

        }

        public override void Load()
        {
            //get the list of files in the directory 
            string[] acctFileList = Directory.GetFiles(_acctDirPath);

            //go through the list of files, create the appropriate accounts and load the file 
            foreach (string acctFileName in acctFileList)
            {
                using (FileStream acctStream = new FileStream(acctFileName, FileMode.Open))
                {
                    //deserialize the account object 
                    DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(UserAccount));
                    UserAccount acct = serializer.ReadObject(acctStream) as UserAccount;

                    //add the account to the list of accounts 
                    _accountList.Add(acct);
                }
            }
        }

        public override void Save()
        {
            //go through each account in the list of accounts and ask it to save itself into a corresponding file 
            foreach (UserAccount acct in _accountList)
            {
                //determine the account file name
                string filePath = $"{_acctDirPath}/Acct{acct.AccountNumber}.json";

                //write the account data to the file 
                using (FileStream acctStream = new FileStream(filePath, FileMode.Create))
                {
                    //serialize the account object 
                    DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(UserAccount));

                    serializer.WriteObject(acctStream, acct);
                }
            }
        }
    }
}
