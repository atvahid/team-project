﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace Sweet_Cafe
{
    public enum AccountFileFormat
    {
        TXT,
        JSON
    }

    class UserAccountSerializer
    {
        //The list of accounts to be saved by the serializer
        protected List<UserAccount> _accountList;

        //The path to the location where the account data will be stored 
        protected string _acctDirPath;

        public UserAccountSerializer(AccountFileFormat fileFormat)
        {
            //the account list is provided by the User object through a property
            _accountList = null;

            //create the file folder where the data will be saved;
            _acctDirPath = Path.Combine(ApplicationData.Current.LocalFolder.Path, "UserData", fileFormat.ToString());
            if (Directory.Exists(_acctDirPath) == false)
            {
                Directory.CreateDirectory(_acctDirPath);
            }
        }

        //Allow clients to provide the account list to be saved / loaded without compromising its visibility and access
        public List<UserAccount> AccountList
        {
            set { _accountList = value; }
        }

        public string AccountDirPath
        {
            get { return _acctDirPath; }
        }

        public virtual void Load()
        {
            throw new InvalidOperationException("Please use a concrete serializer class to load data");
        }

        public virtual void Save()
        {
            throw new InvalidOperationException("Please use a concrete serializer class to save data");
        }
    }

}
